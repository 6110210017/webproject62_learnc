﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnC.Migrations
{
    public partial class IX : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Learn",
                columns: table => new
                {
                    LearnID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShortName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Learn", x => x.LearnID);
                });

            migrationBuilder.CreateTable(
                name: "newsList",
                columns: table => new
                {
                    NewsID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LearnID = table.Column<int>(nullable: false),
                    ReportDate = table.Column<string>(nullable: true),
                    NewsDetail = table.Column<string>(nullable: true),
                    FileWork = table.Column<string>(nullable: true),
                    Heading = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_newsList", x => x.NewsID);
                    table.ForeignKey(
                        name: "FK_newsList_Learn_LearnID",
                        column: x => x.LearnID,
                        principalTable: "Learn",
                        principalColumn: "LearnID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_newsList_LearnID",
                table: "newsList",
                column: "LearnID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "newsList");

            migrationBuilder.DropTable(
                name: "Learn");
        }
    }
}
