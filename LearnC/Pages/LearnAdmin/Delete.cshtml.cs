using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LearnC.Data;
using LearnC.Models;

namespace LearnC.Pages.LearnAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly LearnC.Data.LearnCContext _context;

        public DeleteModel(LearnC.Data.LearnCContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Learn Learn { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Learn = await _context.Learn.FirstOrDefaultAsync(m => m.LearnID == id);

            if (Learn == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Learn = await _context.Learn.FindAsync(id);

            if (Learn != null)
            {
                _context.Learn.Remove(Learn);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
